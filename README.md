# Python-Quiz-repo
### Code ready for others to see

---
#### Quiz

* Quiz is written in Python3 with TKinter and has 50 Disney princess questions or 255 Harry Potter questions
* In the quiz file is the main python script and a seperate folder containg the lists of questions.
* To play see instructions in the game window.
* Two files are in the repo "Quiz main - final.py is used to add or test features or improvements as can see console, "Quiz.pyw" is the game without the console in the background and should be used to play the game.
* In the future I would like to add more lists of questions that self populate the menu? 
---

