from tkinter import *
from tkinter import ttk
import tkinter.messagebox
import string
import random
from lists.hplist import hplist
from lists.dislist import dislist

# Global Variables
noQs = 0  # number of questions choosen from 0 to 24 which means 0 to 25 questions in steps of 5.
score = 0  # Variable that keeps the score.
outof = 0
NUMBER_OF_ATTEMPTS = 2  # Variable that decreases so you get two attempts at each question

list1 = []
list2 = []

# Generate main window and variables needed to be made after main window creation.

win_Height = 400  # variable holds the height of the main window
win_Width = 800  # variable holds the width of the main window

LARGE_FONT = ("Tahoma", 14)  # Variable for font size 18 for windows size 14 for linux.
XL_FONT = ("Verdana", 24)

master = Tk()  # M ain window
menu = Menu(master)

master.wm_title("Childs Quiz")  # Changes the window title.
master.configure(menu=menu, background='purple', pady=10)
master.geometry(str(win_Width) + "x" + str(win_Height))  # sets height and width of the main window.

btnVar = StringVar()  # Dynamic variable that changes on button press (tkinter variable)
answer = StringVar()  # Dynamic variable that changes on change of question, holds the correct answer globaly.(tkinter variable)

Qlabel = ttk.Label(master, text="Start Page", font=LARGE_FONT)  # Create the Question label
Anslabel = ttk.Label(master, text="Start Page", font=LARGE_FONT)  # Create the Possible Answers label
label2 = ttk.Label(master, text="", font=XL_FONT)  # Create the Try again label
Qlabel.configure(background='pink')
Anslabel.configure(background='pink')
label2.configure(background='purple', foreground='pink')

# Defining functions section.

# Function for setting list1 to choosen catergory on menu selection click.


def cat(c_cat):
    global list1
    list1 = c_cat


def question(message, options, correct, attempts=NUMBER_OF_ATTEMPTS):
    '''
    message - string
    options - list
    correct - int (Index of list which holds the correct answer)
    attempts - int
    '''
    global answer
    global score
    optionLetters = string.ascii_lowercase[:len(options)]  # take the length of options, the list in the question function and change the numbers into alphabet characters 4 options changes to abcd.
    Qlabel.config(text=message)  # display question element called message from the quesyion function.
    Anslabel.config(text=' '.join('%s: %s' % (letter, answer) for letter, answer in zip(optionLetters, options)))  # formats the answers for display from the question function.
    label2.config(text="")  # this make the try again label hidden until needed.

    while attempts > 0:
        global btnVar  # bring in global variable btnVar
        Qlabel.pack(side=TOP, pady=10, padx=10)  # Place the label with the question on the screen
        Anslabel.pack(side=TOP, pady=10, padx=10)  # Place the label with the possible answers on the screen

        abutton.wait_variable(btnVar)  # Wait until the variable btnVar changes before carrying on the script.
        response = btnVar.get()  # Set local variable response to global variable btnVar.

        if response == optionLetters[correct]:  # Test the button input against the correct answer
            score += 1
            return True

        else:
            attempts -= 1  # If the button pressed is not the correct answer reduce atempts by 1.
            label2.config(text="Try Again")  # Then display a message to try to answer the question again.
            label2.pack(side=TOP)  # Place the try again label on the screen.
    return False

# This function splits the elments of list2 to display them and send the correct elements to the question function.


def DisplayQs():
    for eaQ in list2:
        message = eaQ[0]
        options = eaQ[1:5]
        answer = eaQ[5]
        question(message, options, answer)

# This function displays the final score at the end of the game and empties the list and nulls the variables to restart the game.


def EndScore():
    global score
    global outof
    global list2
    Qlabel.config(text="You scored " + str(score) + " out of " + str(outof) + " points")  # Dispalys your score at the end of the game
    Anslabel.config(text="Thank You for Playing")
    label2.config(text="")  # Ensure try again label is set to blank when the score and thankyou messages are displayed.
    score = 0  # Reset score to zero
    list2 = []  # Reset list2 to empty
    rand = None

# Function to display message if cateregory not set first


def setcat():
    setcat_txt = "Please select a catergory first"
    tkinter.messagebox.showwarning("Warning!", setcat_txt)


'''
This function is called when you pick a number of questions by clicking it in the file menu, it brings in all the
global variables it needs then tries to set the number of questions global variable to choice bought in from menu click,
same for the outof variable. then it checks if list 2 is empty, if not sets it an empty list. once list2 is empty the
questions are picked randomly by calling randomQs function, then the questions are displayed and the game played by calling
the DisplayQs function. after this the EndScore function is called and the final score is displayed. if the code fails for
a ValueError because no catergory has been picked then a message box is displayed by calling the setcat function.
'''


def Qs(c_noQs, c_outof):
    global noQs
    global outof
    global list2
    global list1

    try:
        noQs = c_noQs
        outof = c_outof
        if len(list2) > 1:
            list2 = []
        else:
            pass
        RandomQs()
        DisplayQs()
        EndScore()
    except ValueError:
        setcat()


'''
RandomQs function takes list1 and picks a question at random, checks to see if list 2 already contains it, if so passes
and tries again when it is not already there it appends the question to list2.
'''


def RandomQs():
    global noQs

    while len(list2) <= noQs:
        rand = int(random.randint(0, len(list1) - 1))
        if (list1[rand]) in list2:
            pass
        else:
            list2.append(list1[rand])

# Function for a message box that gives instructions on how to use the game.


def Instructions():
    inst_txt = "1. Use the Catergory menu to choose the subject of the questions you wish to answer \n2. Use the Game menu to choose the number of questions you wish to answer \n3. click the A, B, C or D buttons to answer the questions."
    tkinter.messagebox.showinfo("Instructions", inst_txt)


# Function for a message box dispalying About text.


def About():
    about_txt = "This Quiz was written by Me for my daughter"
    tkinter.messagebox.showinfo("About", about_txt)


# Function called when an answer button is pressed


def click_ans(c_ans):
    global score  # Bring in global variable score
    global answer  # Bring in global variable answer
    btnVar.set(str(c_ans))  # Changes / sets dynamic btnVar variable to the string value "a, b, c or d depending on which button is clicked"


# Function to quit and exit the game


def Finished():
    master.quit()
    master.destroy()


'''
create catergory menu, display and run commanmd on click. command=lamda: cat() allows the function to take a argument
in the brackets which command = cat will not let you do.
'''

catmenu = Menu(menu)
menu.add_cascade(label="Catergory", menu=catmenu)
catmenu.add_command(label="Disney", command=lambda: cat(dislist))
catmenu.add_separator()
catmenu.add_command(label="Harry Potter", command=lambda: cat(hplist))

# Create the file menu, display options for number of questions you can choose and when clicked run command
filemenu = Menu(menu)
menu.add_cascade(label="Game", menu=filemenu)
filemenu.add_command(label="5 Questions", command=lambda: Qs(4, 5))
filemenu.add_command(label="10 Questions", command=lambda: Qs(9, 10))
filemenu.add_command(label="15 Questions", command=lambda: Qs(14, 15))
filemenu.add_command(label="20 Questions", command=lambda: Qs(19, 20))
filemenu.add_command(label="25 Questions", command=lambda: Qs(24, 25))
filemenu.add_separator()
filemenu.add_command(label="Exit", command=Finished)

# Create help menu which opens message boxes with info on.
helpmenu = Menu(menu)
menu.add_cascade(label="Help", menu=helpmenu)
helpmenu.add_command(label="Instructions", command=Instructions)
helpmenu.add_command(label="About...", command=About)

# Creates a frame at bottom centre of the screen to put the buttons in.
f0 = Frame(master, height=84, width=win_Width / 2)  # Sets height and width of frame.
f0.pack_propagate(1)  # Allow to expand to accept 4 buttons
f0.pack(side=BOTTOM)  # Aligns the frame at the bottom of the main window.

# Creates a frame in the frame f0 and makes it certain height and width and adds padding.
f1 = Frame(f0, height=82, width=132, padx=25, pady=15)  # Creates a frame for the button object abutton, to fill, in farme f0 with a specific height and width in pixels.
f1.pack_propagate(0)  # don't shrink
Frame.configure(f1, background='pink')
f1.pack(side=LEFT)  # Moves the craeted frame about in the frame f0.

# Creates a frame in the frame f0 and makes it certyain height and width and adds padding.
f2 = Frame(f0, height=82, width=132, padx=25, pady=15)  # Creates a frame for the button object bbutton, to fill, in farme f0 with a specific height and width in pixels.
f2.pack_propagate(0)  # don't shrink
Frame.configure(f2, background='pink')
f2.pack(side=LEFT)  # Moves the craeted frame about in the frame f0

# Creates a frame in the frame f0 and makes it certyain height and width and adds padding.
f3 = Frame(f0, height=82, width=132, padx=25, pady=15)  # Creates a frame for the button object cbutton, to fill, in farme f0 with a specific height and width in pixels.
f3.pack_propagate(0)  # don't shrink
Frame.configure(f3, background='pink')
f3.pack(side=LEFT)  # Moves the craeted frame about in the frame f0

# Creates a frame in the frame f0 and makes it certyain height and width and adds padding.
f4 = Frame(f0, height=82, width=132, padx=25, pady=15)  # Creates a frame for the button object cbutton, to fill, in farme f0 with a specific height and width in pixels.
f4.pack_propagate(0)  # Don't shrink
Frame.configure(f4, background='pink')
f4.pack(side=LEFT)  # Moves the craeted frame about in the frame f0

# Button creation
abutton = Button(f1, text="Im A", bg="red", command=lambda: click_ans("a"))
abutton.pack(fill=BOTH, expand=1)  # Makes the button object fill the space of the created frame f1 displays on screen.

bbutton = Button(f2, text="Im B", bg="green", command=lambda: click_ans("b"))
bbutton.pack(fill=BOTH, expand=1)  # Makes the button object fill the space of the created frame f2 displays on screen.

cbutton = Button(f3, text="Im C", bg="red", command=lambda: click_ans("c"))
cbutton.pack(fill=BOTH, expand=1)  # Makes the button object fill the space of the created frame f3 displays on screen.

dbutton = Button(f4, text="Im D", bg="green", command=lambda: click_ans("d"))
dbutton.pack(fill=BOTH, expand=1)  # Makes the button object fill the space of the created frame f3 displays on screen.

mainloop()
